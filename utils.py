from keras.preprocessing.image import ImageDataGenerator
import matplotlib.pyplot as plt

import ipdb
import glob
from PIL import Image
import os, cv2,itertools
import tqdm
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split


import torch
from keras.callbacks import ModelCheckpoint, LearningRateScheduler, EarlyStopping, ReduceLROnPlateau
from torch.utils.data import DataLoader,Dataset


def random():
    np.random.seed(11)
    torch.manual_seed(11)
    torch.cuda.manual_seed(11)

def init():
    imageid_path_dict = {}
    lesion_type = {
        'melanoma',
        'Benign keratosis',
        'Basal cell carcinoma',
        'Melanocytic nevi',
        'Actinic keratoses',
        'Vascular lesions',
        'Dermatofibroma'
    }
    data_dir = '../input'
    all_image_path = glob(os.path.join(data_dir, '*', '*.jpg'))

    for x in all_image_path:
        imageid_path_dict.add(os.path.splitext(os.path.basename(x))[0])


    norm_mean, norm_std = compute_img_mean_std(all_image_path)
    print(norm_std,norm_mean)
def flow_from_dataframe(img_data_gen, raw_df, path_col, y_col, **dflow_args):
    in_df = raw_df.copy()
    in_df[path_col] = in_df[path_col].map(str)
    in_df[y_col] = in_df[y_col].map(lambda x: np.array(x))
    df_gen = img_data_gen.flow_from_dataframe(in_df,
                                              x_col=path_col,
                                              y_col=y_col,
                                    class_mode = 'raw',
                                    **dflow_args)
    df_gen._targets = np.stack(df_gen.labels, 0)
    return df_gen
def test_batch():
    train_gen = flow_from_dataframe(core_idg, train_df,
                                    path_col='image_path',
                                    y_col='dx_id',
                                    target_size=IMG_SIZE,
                                    color_mode='rgb',
                                    batch_size=BATCH_SIZE)
    t_x, t_y = next(train_gen)
    valid_x, valid_y = next(flow_from_dataframe(valid_idg,
                                                valid_df,
                                                path_col='image_path',
                                                y_col='dx_id',
                                                target_size=IMG_SIZE,
                                                color_mode='rgb',
                                                batch_size=VALID_IMG_COUNT))
    weight_path = "{}_weights.best.hdf5".format('skin_cancer_detector')

    checkpoint = ModelCheckpoint(weight_path, monitor='val_loss', verbose=1,
                                 save_best_only=True, mode='min', save_weights_only=True)

    reduceLROnPlat = ReduceLROnPlateau(monitor='val_loss', factor=0.8, patience=10, verbose=1, mode='auto',
                                       epsilon=0.0001, cooldown=5, min_lr=0.0001)
    early = EarlyStopping(monitor="val_loss",
                          mode="min",
                          patience=15)
    callbacks_list = [checkpoint, early, reduceLROnPlat]
    train_gen.batch_size = BATCH_SIZE
    fit_results = skin_model.fit_generator(train_gen,
                                           steps_per_epoch=train_gen.samples // BATCH_SIZE,
                                           validation_data=(valid_x, valid_y),
                                           epochs=EPOCHS,
                                           callbacks=callbacks_list,
                                           workers=3)
    #ipdb.set_trace()
    clear_output()
def compute_img_mean_std(image_paths):


    means, stdevs = [], []
    for i in tqdm(range(len(image_paths))):
        img = cv2.imread(image_paths[i]).resize(img, (224, 224

    imgs = np.stack(imgs, axis=3).astype(np.float32) / 255.

    pixels = imgs[:, :, 0, :].ravel()
    stdevs.append(np.std(pixels))
    means.append(np.mean(pixels))
    pixels = imgs[:, :, 1, :].ravel()
    stdevs.append(np.std(pixels))
    means.append(np.mean(pixels))
    pixels = imgs[:, :, 2, :].ravel()  # resize to one row
    stdevs.append(np.std(pixels))
    means.append(np.mean(pixels))

    return means.reverse(),stdevs.reverse()





def plot(train_loss,val_loss,train_acc,val_acc):
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(20, 10))
    ax1.plot(train_loss, label='Training')
    ax1.plot(val_loss, label='Validation')
    ax1.legend()
    ax1.set_title('Loss')
    ax2.plot(train_acc, label='Training')
    ax2.plot(val_acc, label='Validation')
    ax2.legend()
    ax2.set_title(Accuracy)
    ax2.set_ylim(0, 1)


