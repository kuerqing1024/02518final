import numpy as np
from keras import Model
from keras.preprocessing.image import ImageDataGenerator
from models import backbone
import openslide
from openslide.deepzoom import DeepZoomGenerator
from pandas import HDFStore
import pandas as pd
import glob
from paths import submission_dir
from datasets import load_validation_data, load_test_data
from misc_utils.prediction_utils import cyclic_stacking

def softmax(x):
    e_x = np.exp(x - np.max(x))
    return e_x / np.sum(e_x, axis=1, keepdims=True)

def color_perturbation(image):
      
      image = ImageDataGenerator(brightness_range=64./ 255.)
      image = ImageDataGenerator(saturation_lower=0,upper=0.25)



if __name__ == '__main__':



    def tta_predict(model, img_arr):
        img_arr_tta = cyclic_stacking(img_arr)
        pred_logits = np.zeros(shape=(img_arr.shape[0], 7))

        for _img_crops in img_arr_tta:
            pred_logits += model.predict(_img_crops)

        pred_logits = pred_logits/len(img_arr_tta)

        return pred_logits

    backbone_name = 'inception_v3'
    version = '0'
    use_tta = False

    pred_set = 'validation'  # or test
    load_func = load_validation_data if pred_set == 'validation' else load_test_data
    images, image_names = load_func(task, output_size=224)

    max_num_images = 3000  # Select first 3000 images
    max_num_images = images.shape[0]
    images = images[:max_num_images]
    images = color_perturbation(images)
    image_names = image_names[:max_num_images]

    y_pred = np.zeros(shape=(max_num_images, 7))

    for k_fold in range(num_folds):
        print('Processing fold ', k_fold)
        run_name =backbone_name + '_k' + str(k_fold) + '_v' + version
        model = backbone(backbone_name).classification_model(load_from=run_name)
        predictions_model = Model(inputs=model.input, outputs=model.get_layer('predictions').output)
        if use_tta:
            y_pred += tta_predict(model=predictions_model, img_arr=images)
        else:
            y_pred += predictions_model.predict(images)

    y_pred = y_pred / num_folds
    y_prob = softmax(y_pred)

    for i in in range(max_num_images):
        slides = openslide.ImageSlide(images[i])
        thumbnail = sldes.get_thumbnail((slide.dimensions[0] / 224, slide.dimensions[1] / 224))
    
        thumbnail_grey = np.array(thumbnail.convert('L')) # convert to grayscale
        thresh = threshold_otsu(thumbnail_grey)
        binary = thumbnail_grey > thresh
    
        patches = pd.DataFrame(pd.DataFrame(binary).stack())
        patches['is_tissue'] = ~patches[0]
        patches.drop(0, axis=1, inplace=True)
        patches['slide_path'] = slide_path_t[0]

        with openslide.open_slide(images[i]) as truth:
            thumbnail_truth = truth.get_thumbnail((truth.dimensions[0] / 224, truth.dimensions[1] / 224)) 
        
        y_pred[i] = pd.DataFrame(pd.DataFrame(np.array(thumbnail_truth.convert("L"))).stack())
        y_pred[i] = patches_y[0] > 0
        y_pred[i].drop(0, axis=1, inplace=True)

    #save image

    for i in in range(len(images[0])):
            batch_samples = images.iloc[i]
        
            for _, batch_sample in batch_samples.iterrows():
                 
                with openslide.open_slide(batch_sample.slide_path) as slide:
                    tiles = DeepZoomGenerator(slide, tile_size=224, overlap=0, limit_bounds=False)
                    img = tiles.get_tile(tiles.level_count-1, batch_sample.tile_loc[::-1])
                    im = np.array(img)
                    int1, int2= batch_sample.tile_loc[::-1]
                    if  batch_sample.is_mel==True:
                        imsave('D:\\02518\\datasets\\HAM_10000\\neg\\%s_%d_%d.png' % (os.path.splitext(osp.basename(batch_sample.slide_path))[0], int1, int2), im)
                    else:
                        imsave('D:\\02518\\datasets\\HAM_10000\\pos\\%s_%d_%d.png' % (os.path.splitext(osp.basename(batch_sample.slide_path))[0], int1, int2), im)

            yield

    images_new = pd.concat([patches, y_pred], axis=1)

    num_folds = 5


    
   
